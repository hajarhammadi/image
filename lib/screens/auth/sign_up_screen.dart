import 'package:flutter/material.dart';
import 'widgets/textfiead_item.dart';
import 'widgets/TextformfielModel.dart';
import 'package:carloc2/controllers/validators.dart';
import 'widgets/image.dart';

final formKey = GlobalKey<FormState>();

final emailModel = TextFormFieldModel(
  labelText: "Email",
  validator: validatorEmail,
  hintText: 'Enter your email',
  prefixIcon: const Icon(
    Icons.person,
    color: Color(0xFFFEC601),
  ),
  keyboardType: TextInputType.emailAddress,
  obscureText: false,
);

final PasswordModel = TextFormFieldModel(
  labelText: "Password",
  validator: validatorPassword,
  hintText: 'Enter your password',
  prefixIcon: const Icon(
    Icons.lock,
    color: Color(0xFFFEC601),
  ),
  keyboardType: TextInputType.none,
  obscureText: true,
);

final PasswordConfirmModel = TextFormFieldModel(
  labelText: "Confirm password",
  validator: validatorPasswordConfirmation,
  hintText: 'Confirm your password',
  prefixIcon: const Icon(
    Icons.lock,
    color: Color(0xFFFEC601),
  ),
  keyboardType: TextInputType.none,
  obscureText: true,
);

class SignupScreen extends StatelessWidget {
  void signUp() {
    if (formKey.currentState!.validate()) {
      // Placeholder implementation for user registration logic
      // Replace this with your actual user registration logic
      print("User registered successfully!");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(35),
          child: SingleChildScrollView(
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        onPressed: () {
                          Navigator.pushNamed(context, "/SignIn");
                        },
                        icon: Icon(
                          Icons.arrow_back_ios_new_rounded,
                          size: 16,
                          color: Colors.grey,
                        ),
                      ),
                      Text(
                        'Sign Up',
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Icon(
                        Icons.arrow_back_ios_new_rounded,
                        size: 16,
                        color: Colors.white,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20, // Adjust the height as needed
                  ),
                  CustomImageWidget(),
                  SizedBox(
                    height: 25, // Adjust the height as needed
                  ),
                  TextFormFieldItem(TextformModel: emailModel),
                  SizedBox(height: 20),
                  TextFormFieldItem(
                    TextformModel: PasswordModel,
                  ),
                  SizedBox(height: 20),
                  TextFormFieldItem(
                    TextformModel: PasswordConfirmModel,
                  ),
                  SizedBox(height: 20),
                  TextButton(
                    onPressed: signUp,
                    child: Text('Register'),
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.orange,
                      foregroundColor: Colors.white,
                      padding: EdgeInsets.symmetric(
                        horizontal: 130,
                        vertical: 4,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
