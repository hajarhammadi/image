import 'package:carloc2/screens/auth/widgets/TextformfielModel.dart';
import 'package:flutter/material.dart';

class TextFormFieldItem extends StatelessWidget {
  final TextFormFieldModel TextformModel;

  const TextFormFieldItem({
    Key? key,
    required this.TextformModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: TextformModel.validator,
      keyboardType: TextformModel.keyboardType,
      obscureText: TextformModel.obscureText,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 25),
        labelText: TextformModel.labelText,
        hintText: TextformModel.hintText,
        prefixIcon: TextformModel.prefixIcon,
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
          borderSide: BorderSide(
            color: Color(0xFFFEC601),
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
        ),
      ),
    );
  }
}
