import 'package:flutter/material.dart';
import 'package:carloc2/controllers/validators.dart';

class TextFormFieldModel {
  final String labelText, hintText;
  final Widget prefixIcon;
  final TextInputType keyboardType;
  final bool obscureText;
  final String? Function(String?) validator;

  TextFormFieldModel({
    required this.labelText,
    required this.hintText,
    required this.prefixIcon,
    required this.keyboardType,
    required this.obscureText,
    required this.validator,
  });
}

TextFormFieldModel PasswordConfirmModel = TextFormFieldModel(
  labelText: "Confirm password",
  validator: validatorPasswordConfirmation,
  hintText: 'Confirm your password',
  prefixIcon: const Icon(Icons.lock,
      color: Color(
        0xFFFEC601,
      )),
  keyboardType: TextInputType.none,
  obscureText: true,
);
// Définissez les autres modèles de formulaire de la même manière
