import 'package:flutter/material.dart';

class CustomImageWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 20,
        ),
        Container(
          width: 300, // Largeur du container (ajustez selon vos besoins)
          height: 300, // Hauteur du container (ajustez selon vos besoins)
          decoration: BoxDecoration(
            shape: BoxShape.circle, // Forme circulaire
            image: DecorationImage(
              image:
                  AssetImage('assets/images/sign up.jpg'), // Chemin de l'image
              fit: BoxFit.cover, // Ajustement de l'image dans le container
            ),
          ),
        ),
        SizedBox(
          height: 25,
        ),
      ],
    );
  }
}
