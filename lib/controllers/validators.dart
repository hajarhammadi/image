String? validatorPassword(String? value) {
  if (value!.length < 8) {
    return "The length is < 8";
  } else {
    return null;
  }
}

String? validatorPasswordConfirmation(String? value) {
  if (value!.length < 8) {
    return "The length is < 8";
  } else {
    return null;
  }
}

String? validatorEmail(String? value) {
  final bool emailValid = RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(value!);
  if (!emailValid) {
    return "Email not valid ";
  } else {
    return null;
  }
}
